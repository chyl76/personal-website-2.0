particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 200,
      "density": {
        "enable": true,
        "value_area": 800
      }
    },
    "color": {
      "value": "#000"
    },
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 5,
        "color": "#FFF"
      },
      "polygon": {
        "nb_sides": 5
      }
    },
    "opacity": {
      "value": 1,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 1,
        "opacity_min": 1,
        "sync": false
      }
    },
    "size": {
      "value": 7,
      "random": false,
      "anim": {
        "enable": true,
        "speed": 10,
        "size_min": 0.1,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 325,
      "color": "#1A1A1D",
      "opacity": 1,
      "width": 1.5,
    },
    "move": {
      "enable": true,
      "speed": 30,
      "direction": "none",
      "random": false,
      "straight": false,
      "out_mode": "out",
      "bounce": false,
      "attract": {
        "enable": false,
        "rotateX": 600,
        "rotateY": 1200
      }
    }
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": true,
        "mode": "bubble"
      },
      "onclick": {
        "enable": true,
        "mode": "repulse"
      },
      "resize": true,
    },
    "modes": {
      "grab": {
        "distance": 100,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 300,
        "size": 20,
        "duration": 5,
        "opacity": 0.5,
        "speed": 5,
      },
      "repulse": {
        "distance": 800,
        "duration": 0.1
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});

// Really bad nested function click chain of events
// Need to change to AJAX
$(document).ready(function(){
  $(window).on('click', function(){
    $(".play-text").fadeOut(30, function(){
        $(this).delay(3).text("Go back to work.").fadeIn(1);
    });
    setTimeout(function(){
      $(window).on('click', function(){
        $(".play-text").fadeOut("slow", function(){
            $(this).delay(3).text("You really should go now...").fadeIn(1);
        });
        setTimeout(function(){
          $(window).on('click', function(){
            $(".play-text").fadeOut("slow", function(){
                $(this).delay(3).text("Alright. I am taking you there.").fadeIn(1);
                function BackToWork(){
                  window.location.href = "/work/"
                }
                setTimeout(BackToWork, 8000);
            });
          })
        }, 1000);
      })
    }, 1000);
  });
});
